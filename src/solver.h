class solver
{
public:
   static bool isSolution(int t, int a)
   {
      return t == a;
   }
   static bool isSolutionP(int t, int a, int b, char *buf)
   {
      return sprintf(buf, "%d + %d. ", a, b) && t == a + b                    
         ||  sprintf(buf, "%d - %d. ", a, b) && t == a - b                    
         ||  sprintf(buf, "%d * %d. ", a, b) && t == a * b                    
         ||  sprintf(buf, "%d / %d. ", a, b) && (b && a % b == 0 && t == a / b)
         ;
   }
   static bool isSolution(int t, int a, int b, char* buf)
   {
      return isSolutionP(t, a, b, buf)
         ||  isSolutionP(t, b, a, buf);
   }

   static bool isSolutionP(int t, int a, int b, int c, char* buf)
   {
      return sprintf(buf, "%d + %d, ", a, b) && isSolution(t, a + b, c, buf+strlen(buf))
         ||  sprintf(buf, "%d - %d, ", a, b) && isSolution(t, a - b, c, buf+strlen(buf))
         ||  sprintf(buf, "%d * %d, ", a, b) && isSolution(t, a * b, c, buf+strlen(buf))
         ||  sprintf(buf, "%d / %d, ", a, b) && (b && a % b == 0 && isSolution(t, a / b, c, buf+strlen(buf)))
         ;
   }
   static bool isSolution(int t, int a, int b, int c, char* buf)
   {
      return isSolutionP(t, a, b, c, buf)
         ||  isSolutionP(t, a, c, b, buf)
         ||  isSolutionP(t, b, a, c, buf)
         ||  isSolutionP(t, b, c, a, buf)
         ||  isSolutionP(t, c, a, b, buf)
         ||  isSolutionP(t, c, b, a, buf)
         ;
   }

   static bool isSolutionP(int t, int a, int b, int c, int d, char* buf)
   {
      return sprintf(buf, "%d + %d, ", a, b) && isSolution(t, a + b, c, d, buf+strlen(buf))                    
         ||  sprintf(buf, "%d - %d, ", a, b) && isSolution(t, a - b, c, d, buf+strlen(buf))                    
         ||  sprintf(buf, "%d * %d, ", a, b) && isSolution(t, a * b, c, d, buf+strlen(buf))                    
         ||  sprintf(buf, "%d / %d, ", a, b) && (b && a % b == 0 && isSolution(t, a / b, c, d, buf+strlen(buf)))
         ;
   }

   static bool isSolution(int t, int a, int b, int c, int d, char* buf)
   {
      return isSolutionP(t, a, b, c, d, buf)
         ||  isSolutionP(t, a, b, d, c, buf)
         ||  isSolutionP(t, a, c, b, d, buf)
         ||  isSolutionP(t, a, c, d, b, buf)
         ||  isSolutionP(t, a, d, b, c, buf)
         ||  isSolutionP(t, a, d, c, b, buf)
                                     
         ||  isSolutionP(t, b, a, c, d, buf)
         ||  isSolutionP(t, b, a, d, c, buf)
         ||  isSolutionP(t, b, c, a, d, buf)
         ||  isSolutionP(t, b, c, d, a, buf)
         ||  isSolutionP(t, b, d, a, c, buf)
         ||  isSolutionP(t, b, d, c, a, buf)
                                    
         ||  isSolutionP(t, c, a, b, d, buf)
         ||  isSolutionP(t, c, a, d, b, buf)
         ||  isSolutionP(t, c, b, a, d, buf)
         ||  isSolutionP(t, c, b, d, a, buf)
         ||  isSolutionP(t, c, d, a, b, buf)
         ||  isSolutionP(t, c, d, b, a, buf)
                                   
         ||  isSolutionP(t, d, a, b, c, buf)
         ||  isSolutionP(t, d, a, c, b, buf)
         ||  isSolutionP(t, d, b, a, c, buf)
         ||  isSolutionP(t, d, b, c, a, buf)
         ||  isSolutionP(t, d, c, a, b, buf)
         ||  isSolutionP(t, d, c, b, a, buf)
         ;
   }
};
