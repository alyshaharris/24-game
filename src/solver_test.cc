#include <gtest/gtest.h>
#include "solver.h"

TEST(solver_test, one) {
   char buf[256];
   ASSERT_TRUE(solver::isSolution(5, 5));
   ASSERT_TRUE(solver::isSolution(1, 1));
   ASSERT_TRUE(solver::isSolution(-1, -1));
   ASSERT_FALSE(solver::isSolution(1, -1));
   ASSERT_FALSE(solver::isSolution(50, 49));
};

TEST(solver_test, two) {
   char buf[256];
   ASSERT_TRUE(solver::isSolution(5, 5, 1, buf));
   ASSERT_TRUE(solver::isSolution(2, 1, 1, buf));
   ASSERT_TRUE(solver::isSolution(7, 9, 2, buf));
   ASSERT_TRUE(solver::isSolution(7, 2, 9, buf));
   ASSERT_TRUE(solver::isSolution(7, 49, 7, buf));
   ASSERT_TRUE(solver::isSolution(7, 7, 49, buf));
   ASSERT_FALSE(solver::isSolution(1, -1, 5, buf));
   ASSERT_FALSE(solver::isSolution(50, 49, 6, buf));
};

TEST(solver_test, three) {
   char buf[256];
   ASSERT_TRUE(solver::isSolution(6, 5, 1, 1, buf));
   ASSERT_TRUE(solver::isSolution(3, 1, 1, 1, buf));
   ASSERT_TRUE(solver::isSolution(7, 9, 1, 1, buf));
   ASSERT_TRUE(solver::isSolution(14, 2, 9, 2, buf));
   ASSERT_TRUE(solver::isSolution(3, 49, 7, 4, buf));
   ASSERT_TRUE(solver::isSolution(7, 7, 49, buf));
   ASSERT_FALSE(solver::isSolution(346, -1, 5, 70, buf));
   ASSERT_FALSE(solver::isSolution(50, 39, 6, 9, buf));
};

TEST(solver_test, four) {
   char buf[256];
   ASSERT_TRUE(solver::isSolution(24, 11, 3, 3, 1, buf));
   ASSERT_TRUE(solver::isSolution(24, 4, 4, 2, 1, buf));
   ASSERT_TRUE(solver::isSolution(24, 1, 2, 4, 4, buf));
   ASSERT_TRUE(solver::isSolution(24, 13, 12, 19, 5, buf));
   ASSERT_TRUE(solver::isSolution(0, 0, 50, 60, 66, buf));
   ASSERT_FALSE(solver::isSolution(632, -1, 5, 70, -432, buf));
   ASSERT_FALSE(solver::isSolution(50, 50, 1, 10, 13, buf));
   ASSERT_TRUE(solver::isSolution(24, 3, 4, 10, 12, buf));
};
